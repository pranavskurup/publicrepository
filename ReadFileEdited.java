

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

public class ReadFileEdited {
	public static String readFile() {

		File file = new File("xxxxxx.txt");
		FileInputStream fis = null;

		try {
			/*Reading file using File input stream*/
			StringBuilder builder = new StringBuilder();

			fis = new FileInputStream(file);
			int content;

			while ((content = fis.read()) != -1) {
				// convert to char and append it to StringBuilder
				builder.append((char) content);
			}
			return builder.toString();
		} catch (IOException e) {

			/*We handle exception here and return value appropriately*/
			e.printStackTrace();
			return null;
		} finally {
			/*If file stream is opened, we have to close it to avoid memory leakage*/
			try {
				if (fis != null)
					fis.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
            
            return "FINALLY";
		}
	}


	public static void main(String[] args) {
		String text=ReadFileEdited.readFile();
		System.out.println(text);
	}
}